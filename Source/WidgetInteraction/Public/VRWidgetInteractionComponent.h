// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetInteractionComponent.h"
#include "Components/StaticMeshComponent.h"

#include "VRWidgetInteractionComponent.generated.h"

/**
 * 
 */
UCLASS()
class WIDGETINTERACTION_API UVRWidgetInteractionComponent : public UWidgetInteractionComponent
{
	GENERATED_BODY()
public:
	UVRWidgetInteractionComponent();

	void Init(USceneComponent* parent);

	void SetVisibility(bool visible);

protected:
	void OnFire(bool val);

	UPROPERTY(VisibleAnywhere) UStaticMeshComponent* InteractionRay;

	DECLARE_DELEGATE_OneParam(FFireDelegate, bool);
};
