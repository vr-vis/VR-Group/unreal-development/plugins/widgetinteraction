// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VirtualRealityPawn.h"
#include "VRWidgetInteractionComponent.h"
#include "Modules/ModuleManager.h"

DECLARE_LOG_CATEGORY_EXTERN(WidgetIntLog, Log, All);

class WIDGETINTERACTION_API FWidgetInteractionModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	UFUNCTION() void OnWorldTickStart(ELevelTick, float);

	UVRWidgetInteractionComponent* GetWidgetInteractionComponent();

private:
	void CreateWidgetInteraction(USceneComponent* parent, AVirtualRealityPawn* outer);

private:
	TBaseDelegate<void, ELevelTick, float> on_world_tick_start_delegate_;

	UVRWidgetInteractionComponent* widget_interaction_cmp_;
	UWorld* last_world;
};
