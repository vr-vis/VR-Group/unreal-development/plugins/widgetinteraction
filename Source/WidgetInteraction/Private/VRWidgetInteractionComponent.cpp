// Fill out your copyright notice in the Description page of Project Settings.

#include "VRWidgetInteractionComponent.h"

#include "Runtime/InputCore/Classes/InputCoreTypes.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "ConstructorHelpers.h"
#include "Modules/ModuleManager.h"
#include "Interfaces/IPluginManager.h"
#include "VirtualRealityPawn.h"


UVRWidgetInteractionComponent::UVRWidgetInteractionComponent() {
	InteractionRay = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Interaction Ray"));

	//this ray model as a inlayed cross with flipped normals so it can be seen as a cross in desktop mode where the right hand is attached to the head
	ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("/WidgetInteraction/Ray_Mesh"));
	if (MeshAsset.Object != nullptr)
	{
		InteractionRay->SetStaticMesh(MeshAsset.Object);
	}

	bShowDebug = false;
}

void UVRWidgetInteractionComponent::Init(USceneComponent* parent)
{
	SetVisibility(true);
	InteractionDistance = 1000000.0f;

	auto input_cmp = dynamic_cast<UInputComponent*>(GetOwner()->GetComponentByClass(UInputComponent::StaticClass()));

	input_cmp->BindAction<FFireDelegate>("Fire", IE_Pressed, this, &UVRWidgetInteractionComponent::OnFire, true);
	input_cmp->BindAction<FFireDelegate>("Fire", IE_Released, this, &UVRWidgetInteractionComponent::OnFire, false);

	RegisterComponent();
	InteractionRay->RegisterComponent();

	if (parent != nullptr) {
		InteractionRay->AttachToComponent(parent, FAttachmentTransformRules::KeepRelativeTransform);
		this->AttachToComponent(parent, FAttachmentTransformRules::KeepRelativeTransform);
	}
}

void UVRWidgetInteractionComponent::SetVisibility(bool visible) {
  InteractionRay->SetVisibility(visible);
}

void UVRWidgetInteractionComponent::OnFire(bool val)
{
	if (val == true)
		PressPointerKey(EKeys::LeftMouseButton);
	else
		ReleasePointerKey(EKeys::LeftMouseButton);
}
